#!/usr/bin/perl

use strict;

use File::Basename;
use YAML;
use Data::Dumper;
use IPC::Cmd qw[can_run run run_forked];
use Cwd;
use File::Basename;
use File::stat;
use Time::localtime;
use DateTime;
use Text::CSV qw( csv );
use Getopt::Long;
use Hash::Merge;
my $merge = Hash::Merge->new();

my $config = {};

my $default_yaml = join('', <DATA>);
my $default_config = Load($default_yaml);

$config->{default_config} = $default_config;

my $cl_options = {};
$cl_options->{cfg} = "cfg.yaml"; # default cfg
$cl_options->{d}   = 0;
$cl_options->{h}   = 0;

GetOptions( "cfg=s" => \$cl_options->{cfg},
            "d"     => \$cl_options->{d},
            "h"     => \$cl_options->{h} );

if($cl_options->{h}){
    show_help($config);
    die;
}

init_config($config, $cl_options);

# ____   ____ ____  ___ ____ _____ 
#/ ___| / ___|  _ \|_ _|  _ \_   _|
#\___ \| |   | |_) || || |_) || |  
# ___) | |___|  _ < | ||  __/ | |  
#|____/ \____|_| \_\___|_|    |_|  
                                  
my $file_name = shift;
open FILE, $file_name or die "Couldn't open $file_name $!\n";
my $text = [ <FILE> ];
close FILE;

for my $line (@{$text}){

    my @words = split(' ', $line);
    my $first_word = $words[0];

    if($first_word =~ /[0-9]?[A-Z][a-z]+:$/
    or $first_word =~ /[0-9]+$/){
        print "$first_word\n";
        next;
    }

    print "    - [ ";

    for(@words){
        chomp($_);
        print "\"$_\", ";
    }

    print "]\n";

}

# ____  _   _ ____ ____  
#/ ___|| | | | __ ) ___| 
#\___ \| | | |  _ \___ \ 
# ___) | |_| | |_) |__) |
#|____/ \___/|____/____/ 

sub init_config{

    my ($config, $cl_options) = @_;

    my $cfg_yaml = $config->{default_config}{default_cfg_yaml};
    my $cfg_yaml_file;
    if($config->{default_config}{use_cfg_yaml}){
        $cfg_yaml_file = load_yaml_file($cfg_yaml);
        $config->{cfg_yaml} = $cfg_yaml_file;
    }

    $config->{cl_options} = $cl_options;

    $config->{options} = {};
    $config->{options} = $merge->merge( $config->{options}, $default_config->{cl_options} );
    $config->{options} = $merge->merge( $config->{options}, $cl_options );

}

sub show_help{
    my $config = shift;
    print $config->{default_config}{help};
}

sub strprn{ $_[0] .= $_[1]; }

sub slurp_str{
    my $text = join('', slurp_lines(shift));
    return $text;
}

sub slurp_lines{
    my $file_name = shift;
    open FILE, $file_name or die "Couldn't open $file_name $!\n";
    my $text = [ <FILE> ];
    chomp for @{$text};
    return $text;
}


sub load_yaml_file{
    my $file_name = shift;
    open YAML_FILE, $file_name or die "Couldn't open $file_name $!\n";
    my $text = join('', <YAML_FILE>);
    return Load($text);
}


sub load_csv_file{

    my $file = shift;

    my $csv = Text::CSV->new ({
      binary    => 1,
      auto_diag => 1,
      sep_char  => ','    # change if you have some other separator
    });

    open(my $data, '<:encoding(utf8)', $file) or die "Could not open '$file' $!\n";

    my $csv_lines=[];

    while (my $fields = $csv->getline( $data )) {
        push @{$csv_lines}, $fields;
    }

    close $data;
    return $csv_lines;

}


# Clayton Bauman 2021

__DATA__
---
# default_config yaml:
usage: |
    % perl blank.pl <opts> infile
help: |
    A help message goes here...
#cl_options:
default_cfg_yaml: "cfg.yaml"
use_cfg_yaml: 0
defaults:
    foo: "bar"
    bee: "bop"


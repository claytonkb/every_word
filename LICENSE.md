Material authored by the owner of this repository
-------------------------------------------------

Except for material derived from restricted sources, all other work in this
repository is free to use as you see fit (public domain). Be aware that, under
the licensing of the source materials, you **may not** charge (print, copy, or
otherwise) for the materials under the /html and /src directores of this
repository. In addition, you **may not** use the materials under the /html and
/src directories of this repository in derivative commercial works (for sale).
You **may** freely print or copy any materials in this repository and use them
directly for your personal use, or give them away for personal use. For all
other uses, you should contact the license-holders of the source materials to
inquire about licensing terms and restrictions.

[CC0 1.0 Universal (CC0 1.0)](ttps://creativecommons.org/publicdomain/zero/1.0/)

Public Domain Dedication

This is a human-readable summary of the Legal Code [read the full text](https://creativecommons.org/publicdomain/zero/1.0/legalcode)

No Copyright

    The person who associated a work with this deed has dedicated the work to
    the public domain by waiving all of his or her rights to the work worldwide
    under copyright law, including all related and neighboring rights, to the
    extent allowed by law.

    You can copy, modify, distribute and perform the work, even for commercial
    purposes, all without asking permission. See Other Information below.

This license is acceptable for Free Cultural Works.

Other Information

    In no way are the patent or trademark rights of any person affected by CC0, nor
    are the rights that other persons may have in the work or in how the work is
    used, such as publicity or privacy rights.

    Unless expressly stated otherwise, the person who associated a work with this
    deed makes no warranties about the work, and disclaims liability for all uses
    of the work, to the fullest extent permitted by applicable law.

    When using or citing the work, you should not imply endorsement by the author
    or the affirmer.

Material derived from restricted sources
----------------------------------------

Parts of the works in this repository are derived from sources that permit reuse
under restricted terms.

[Lexham English Bible](https://lexhampress.com/)

The Lexham English Bible is free to print and copy at no-charge, but you must
contact Lexham Press for [licensing information](https://lexhampress.com/LEB-License)
for any other uses.

The derived work in /html/leb directory this repository **is not** a
word-for-word copy of the Lexham English Bible. There are some edits that have
been made to the LEB which are described and explained in /html/leb/CHANGES

This repository and its owner are not associated with the Lexham English Bible
or Lexham Press.

[OpenBible.info](https://www.openbible.info/)

Parts of the cross-reference data available available from this source are used
in the materials in this repository.

[Berean Study Bible](https://berean.bible/downloads.htm)

Some of the free resources available from berean.bible were consulted during the
construction of the materials in this repository but they are not a derivative
work.

Other free/PD/unlicense materials were consulted from the following sources:

- archive.org
- gutenberg.org
- sacred-texts.com


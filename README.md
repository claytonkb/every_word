# every_word

![reader_sample](img/loaves_and_fishes.png)

Several versions of the Bible formatted as HTML with cross-references and other
features. Source data included. Spirit of the license is CC0 (do whatever you
want) but there are some restrictions due to source data, see LICENSE for
licensing details.

![reader_sample](img/reader_sample.jpg)

